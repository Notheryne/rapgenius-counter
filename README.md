# RapGenius counter

A bunch of script to scrape rap-genius, manage the lyrics and count occurences.

Be sure to have lyricsgenius and prettytable installed.

[lyricsgenius](https://github.com/johnwmillr/LyricsGenius)
[prettytable](https://pypi.org/project/PrettyTable/)